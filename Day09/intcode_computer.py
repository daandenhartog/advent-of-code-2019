class Intcode_computer:

	memory = []
	relative_base_offset = 0

	def run_program(self, memory, adress):
		self.memory = memory

		self.solve_opcode_instruction(adress)

	def get_opcode_instruction(self, opcode_raw):
		opcode_length = len(opcode_raw)

		if opcode_length == 1 or opcode_length == 2:
			return int(opcode_raw)

		return int(opcode_raw[opcode_length - 2:opcode_length])

	def get_opcode_parameter_modes(self, opcode_raw):
		opcode_length = len(opcode_raw)

		first_mode = 0
		if opcode_length >= 3:
			first_mode = int(opcode_raw[opcode_length - 3])

		second_mode = 0
		if opcode_length >= 4:
			second_mode = int(opcode_raw[opcode_length - 4])

		third_mode = 0
		if opcode_length >= 5:
			third_mode = int(opcode_raw[opcode_length - 5])

		return [first_mode, second_mode, third_mode]

	def write_to_memory(self, value, adress):
		# memory_count = len(self.memory)
		# if adress >= memory_count:
		# 	for i in range(memory_count, adress + 1):
		# 		self.memory.append('0')

		self.memory[adress] = str(value)

	def get_from_memory(self, adress):
		if adress >= len(self.memory):
			return 0
		else:
			return int(self.memory[adress])

	def get_raw_value(self, adress):
		return int(self.memory[adress])

	def get_mode_value(self, adress, mode):
		raw_value = self.get_raw_value(adress)

		if mode == 0:
			return int(self.memory[raw_value])
		elif mode == 1:
			return raw_value

	def solve_opcode_instruction(self, instruction_pointer_index):
		opcode_instruction = self.get_opcode_instruction(self.memory[instruction_pointer_index])
		opcode_parameter_modes = self.get_opcode_parameter_modes(self.memory[instruction_pointer_index])

		# Finish and halt immediately.
		if opcode_instruction == 99:
			return

		# Addition.
		if opcode_instruction == 1:
			first_value = self.get_mode_value(instruction_pointer_index + 1, opcode_parameter_modes[0])
			second_value = self.get_mode_value(instruction_pointer_index + 2, opcode_parameter_modes[1])
			third_value = self.get_raw_value(instruction_pointer_index + 3)

			self.write_to_memory(first_value + second_value, third_value)
			instruction_pointer_index += 4

		# Multiplication.
		elif opcode_instruction == 2:
			first_value = self.get_mode_value(instruction_pointer_index + 1, opcode_parameter_modes[0])
			second_value = self.get_mode_value(instruction_pointer_index + 2, opcode_parameter_modes[1])
			third_value = self.get_raw_value(instruction_pointer_index + 3)

			self.write_to_memory(first_value * second_value, third_value)
			instruction_pointer_index += 4

		# Take an integer as input and save it to the first parameter.
		elif opcode_instruction == 3:
			first_input = self.get_raw_value(instruction_pointer_index + 1)

			self.write_to_memory(input("Enter value: "), first_input)
			instruction_pointer_index += 2

		# Print the value of the first parameter.
		elif opcode_instruction == 4:
			first_input = self.get_mode_value(instruction_pointer_index + 1, opcode_parameter_modes[0])

			print(first_input)
			instruction_pointer_index += 2

		# If the first parameter is non-zero, it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing.
		elif opcode_instruction == 5:
			first_input = self.get_mode_value(instruction_pointer_index + 1, opcode_parameter_modes[0])
			second_value = self.get_mode_value(instruction_pointer_index + 2, opcode_parameter_modes[1])

			if first_input != 0:
				instruction_pointer_index = second_value
			else:
				instruction_pointer_index += 3

		# If the first parameter is zero, it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing.
		elif opcode_instruction == 6:
			first_input = self.get_mode_value(instruction_pointer_index + 1, opcode_parameter_modes[0])
			second_value = self.get_mode_value(instruction_pointer_index + 2, opcode_parameter_modes[1])

			if first_input == 0:
				instruction_pointer_index = second_value
			else:
				instruction_pointer_index += 3

		# If the first parameter is less than the second parameter, it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
		elif opcode_instruction == 7:
			first_value = self.get_mode_value(instruction_pointer_index + 1, opcode_parameter_modes[0])
			second_value = self.get_mode_value(instruction_pointer_index + 2, opcode_parameter_modes[1])
			third_value = self.get_raw_value(instruction_pointer_index + 3)

			self.write_to_memory(1 if first_value < second_value else 0, third_value)
			instruction_pointer_index += 4

		# If the first parameter is equal to the second parameter, it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
		elif opcode_instruction == 8:
			first_value = self.get_mode_value(instruction_pointer_index + 1, opcode_parameter_modes[0])
			second_value = self.get_mode_value(instruction_pointer_index + 2, opcode_parameter_modes[1])
			third_value = self.get_raw_value(instruction_pointer_index + 3)

			self.write_to_memory(1 if first_value == second_value else 0, third_value)
			instruction_pointer_index += 4

		self.solve_opcode_instruction(instruction_pointer_index)