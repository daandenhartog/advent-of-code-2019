# https://adventofcode.com/2019/day/5

from intcode_computer import Intcode_computer

def get_inputs():
	inputs = []
	with open('inputs.txt') as file:
		for line in file:
			current_line = line.split(',')
			for value in current_line:
				inputs.append(value)
	return inputs

def assignment_a():
	computer = Intcode_computer()
	computer.run_program(get_inputs(), 0)

# def assignment_b():
# 	Intcode_computer.alter_input_by_opcode(get_inputs(), 0)

assignment_a()
# assignment_b()