from Point import Point

class Line:
	
	start = Point(0, 0)
	end = Point(0, 0)

	is_horizontal = False
	is_vertical = False

	def __init__(self, start, end):
		self.start = start
		self.end = end

		self.is_horizontal = self.start.y == self.end.y
		self.is_vertical = self.start.x == self.end.x

	def get_intersetion_point(self, line):
		if self.is_horizontal == line.is_horizontal or self.is_vertical == line.is_vertical:
			return None

		horizontal_line = self if self.is_horizontal else line
		vertical_line = self if self.is_vertical else line

		smallest_horizontal_x = horizontal_line.get_smallest_x()
		biggest_horizontal_x = horizontal_line.get_biggest_x()
		vertical_x = vertical_line.get_smallest_x()

		if vertical_x < smallest_horizontal_x or vertical_x > biggest_horizontal_x:
			return None

		smallest_vertical_x = vertical_line.get_smallest_y()
		biggest_vertical_x = vertical_line.get_biggest_y()
		horizontal_y = horizontal_line.get_smallest_y()

		if horizontal_y < smallest_vertical_x or horizontal_y > biggest_vertical_x:
			return None

		return Point(vertical_x, horizontal_y)

	def get_steps_to_position(self, position):
		if position.x < self.get_smallest_x() or position.x > self.get_biggest_x() or position.y < self.get_smallest_y() or position.y > self.get_biggest_y():
			return -1

		return abs(position.x - self.start.x) + abs(position.y - self.start.y)

	def get_step_length(self):
		return abs((self.end.x - self.start.x) + (self.end.y - self.start.y))

	def get_smallest_x(self):
		return self.start.x if self.start.x < self.end.x else self.end.x

	def get_biggest_x(self):
		return self.start.x if self.start.x > self.end.x else self.end.x

	def get_smallest_y(self):
		return self.start.y if self.start.y < self.end.y else self.end.y

	def get_biggest_y(self):
		return self.start.y if self.start.y > self.end.y else self.end.y

	def __str__(self):
		return f'Line({self.start}, {self.end})'