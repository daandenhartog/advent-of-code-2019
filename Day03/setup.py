# https://adventofcode.com/2019/day/3

from Point import Point
from Line import Line

def setup_wires():
	inputsA = ''
	InputsB = ''
	with open('PuzzleInputs.txt') as file:
		inputsA = file.readline().split(',')
		InputsB = file.readline().split(',')

	setup_wire_coordinates(wire_coordinates_a, inputsA)
	setup_wire_coordinates(wire_coordinates_b, InputsB)

def setup_wire_coordinates(refence, inputs):
	start_position = Point(0, 0)
	for input in inputs:
		direction = code_to_direction(input)
		end_position = start_position.add(direction)

		refence.append(Line(start_position, end_position))
		start_position = end_position		

def code_to_direction(code):
	direction = code[0]
	distance = int(code[1:len(code)])

	if direction == 'L':
		return Point(-distance, 0)
	if direction == 'R':
		return Point(distance, 0)
	if direction == 'U':
		return Point(0, distance)
	if direction == 'D':
		return Point(0, -distance)

def assignment_a():
	smallest_manhattan_distance = float("inf")

	for a in wire_coordinates_a:
		for b in wire_coordinates_b:
			intersection = a.get_intersetion_point(b)
			if intersection == None or intersection == Point(0, 0):
				continue

			manhattan_distance = Point(0, 0).get_manhattan_distance(intersection)
			if manhattan_distance < smallest_manhattan_distance:
				smallest_manhattan_distance = manhattan_distance

	return smallest_manhattan_distance

def get_steps_to_position(line, position, lines):
	step_count = line.get_steps_to_position(position)

	for l in lines:
		if l == line:
			break

		step_count += l.get_step_length()

	return step_count

def assignment_b():
	lowest_step_count = float("inf")

	for a in wire_coordinates_a:
		for b in wire_coordinates_b:
			intersection = a.get_intersetion_point(b)
			if intersection == None or intersection == Point(0, 0):
				continue

			step_count = get_steps_to_position(a, intersection, wire_coordinates_a) + get_steps_to_position(b, intersection, wire_coordinates_b)
			if step_count < lowest_step_count:
				lowest_step_count = step_count

	return lowest_step_count

wire_coordinates_a = []
wire_coordinates_b = []

setup_wires()
print(assignment_a())
print(assignment_b())