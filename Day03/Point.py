class Point:
	
	x = 0
	y = 0

	def __init__(self, x, y):
		self.x = x
		self.y = y

	def add(self, point):
		return Point(self.x + point.x, self.y + point.y)

	def get_manhattan_distance(self, other):
		return abs(self.x - other.x) + abs(self.y - other.y)

	def __str__(self):
		return f'Point({self.x}, {self.y})'

	def __eq__(self, other):
		if other is None:
			return False

		return self.x == other.x and self.y == other.y