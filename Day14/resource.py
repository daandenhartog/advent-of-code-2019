class Resource():

	identifier = ''
	count = 0

	def __init__(self, identifier, count):
		self.identifier = identifier
		self.count = count

	def __str__(self):
		return f'Resource({self.identifier}, {self.count})'

	def __eq__(self, other):
		return self.identifier == other.identifier