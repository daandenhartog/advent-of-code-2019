from recipe import Recipe
from resource import Resource

def get_resouce_from_string(string):
	parts = string.split(' ')
	resource = Resource(parts[1], int(parts[0]))
	return resource

def get_resources_from_strings(strings):
	resources = []
	for string in strings:
		resources.append(get_resouce_from_string(string))

	return resources

def setup_recipes():
	with open('inputs.txt') as file:
		for line in file:
			parts = line.split(' => ')
			inputs = get_resources_from_strings(parts[0].split(', '))
			output = get_resouce_from_string(parts[1].replace('\n', ''))

			recipes.append(Recipe(inputs, output))

def get_recipe_with_output(identifier):
	for recipe in recipes:
		if recipe.output.identifier == identifier:
			return recipe

	return None

def get_start_resources():
	identifier_to_count = {
		start_identifier : 0
	}

	for recipe in recipes:
		identifier_to_count[recipe.output.identifier] = 0

	identifier_to_count[goal_identifier] = -1

	while True:
		has_altered = False

		for identifier in identifier_to_count:
			if identifier_to_count[identifier] < 0 and identifier != start_identifier:
				recipe = get_recipe_with_output(identifier)

				identifier_to_count[identifier] += recipe.output.count
				for input in recipe.inputs:
					identifier_to_count[input.identifier] -= input.count

				has_altered = True

		if has_altered == False:
			break

	return identifier_to_count[start_identifier] * -1

def assignment_a():
	print(get_start_resources())

goal_identifier = 'FUEL'
start_identifier = 'ORE'
recipes = []

setup_recipes()
assignment_a()