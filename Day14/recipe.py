class Recipe():

	inputs = []
	output = None

	def __init__(self, inputs, output):
		self.inputs = inputs
		self.output = output

	def __str__(self):
		return f'Recipe({self.inputs}, {self.output})'