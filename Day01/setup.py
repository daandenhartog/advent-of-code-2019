# https://adventofcode.com/2019/day/1

def get_inputs():
	inputs = []
	with open('PuzzleInputs.txt') as file:
		for line in file:
			inputs.append(int(line))
	return inputs

def get_mass_to_fuel(mass):
	fuel = int(mass / 3)
	fuel -= 2
	return fuel

def get_fuel_requirement(mass):
	fuel = get_mass_to_fuel(mass)
	total_fuel = fuel

	while fuel > 0:
		fuel = get_mass_to_fuel(fuel)
		if fuel > 0:
			total_fuel += fuel

	return total_fuel

def get_total_fuel_requirement(inputs):
	total_fuel_requirement = 0
	for input in inputs:
		total_fuel_requirement += get_fuel_requirement(input)
	return total_fuel_requirement

print(get_total_fuel_requirement(get_inputs()))