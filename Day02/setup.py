# https://adventofcode.com/2019/day/2

def get_inputs():
	inputs = []
	with open('PuzzleInputs.txt') as file:
		for line in file:
			current_line = line.split(',')
			for value in current_line:
				inputs.append(int(value))
	return inputs

def get_inputs_altered_by_opcode(inputs, opcode_index):
	opcode = inputs[opcode_index]
	if opcode == 1 or opcode == 2:
		first_index = inputs[opcode_index + 1]
		first_value = inputs[first_index]

		second_index = inputs[opcode_index + 2]
		second_value = inputs[second_index]

		third_index = inputs[opcode_index + 3]
		third_value = first_value + second_value if opcode == 1 else first_value * second_value

		inputs[third_index] = third_value
		return get_inputs_altered_by_opcode(inputs, opcode_index + 4)

	return inputs

def print_assignment_a(inputs):
	inputs[1] = 12
	inputs[2] = 2
	print(get_inputs_altered_by_opcode(inputs, 0)[0])

def print_assingment_b(inputs):
	for x in range(100):
		for y in range(100):
			input = inputs.copy()
			input[1] = x
			input[2] = y

			if get_inputs_altered_by_opcode(input, 0)[0] == 19690720:
				print(100 * x + y)

inputs = get_inputs()
print_assignment_a(inputs.copy())
print_assingment_b(inputs.copy())