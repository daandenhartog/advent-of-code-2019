class Intcode_computer(object):

	@staticmethod
	def get_opcode_instructions(opcode):
		opcode_identifier_length = len(opcode)
		opcode_identifier = 0

		if opcode_identifier_length == 1:
			opcode_identifier = int(opcode)
		else:
			opcode_identifier = int(opcode[opcode_identifier_length - 2:opcode_identifier_length])

		opcode_first_parameter = 0
		if opcode_identifier_length >= 3:
			opcode_first_parameter = int(opcode[opcode_identifier_length - 3])

		opcode_second_parameter = 0
		if opcode_identifier_length >= 4:
			opcode_second_parameter = int(opcode[opcode_identifier_length - 4])

		opcode_third_parameter = 0
		if opcode_identifier_length >= 5:
			opcode_third_parameter = int(opcode[opcode_identifier_length - 5])

		return [opcode_identifier, opcode_first_parameter, opcode_second_parameter, opcode_third_parameter]

	@staticmethod
	def get_value_based_on_parameter_mode(inputs, adress, mode):
		if mode == 0:
			return int(inputs[adress])
		if mode == 1:
			return adress

	@staticmethod
	def alter_input_by_opcode(inputs, instruction_index):
		opcode_raw_values = inputs[instruction_index:instruction_index + 4]
		opcode_instructions = Intcode_computer.get_opcode_instructions(opcode_raw_values[0])

		opcode_instruction = opcode_instructions[0]
		instruction_pointer_index = instruction_index

		if opcode_instruction == 99:
			return

		if opcode_instruction == 1:
			first_value = Intcode_computer.get_value_based_on_parameter_mode(inputs, int(opcode_raw_values[1]), opcode_instructions[1])
			second_value = Intcode_computer.get_value_based_on_parameter_mode(inputs, int(opcode_raw_values[2]), opcode_instructions[2])
			inputs[int(opcode_raw_values[3])] = str(first_value + second_value)
			instruction_pointer_index += 4

		elif opcode_instruction == 2:
			first_value = Intcode_computer.get_value_based_on_parameter_mode(inputs, int(opcode_raw_values[1]), opcode_instructions[1])
			second_value = Intcode_computer.get_value_based_on_parameter_mode(inputs, int(opcode_raw_values[2]), opcode_instructions[2])
			inputs[int(opcode_raw_values[3])] = str(first_value * second_value)
			instruction_pointer_index += 4

		elif opcode_instruction == 3:
			adress = int(opcode_raw_values[1])
			inputs[adress] = input("Enter value: ")
			instruction_pointer_index += 2

		elif opcode_instruction == 4:
			adress = Intcode_computer.get_value_based_on_parameter_mode(inputs, int(opcode_raw_values[1]), opcode_instructions[1])
			print(adress)
			instruction_pointer_index += 2

		elif opcode_instruction == 5:
			first_value = Intcode_computer.get_value_based_on_parameter_mode(inputs, int(opcode_raw_values[1]), opcode_instructions[1])
			second_value = Intcode_computer.get_value_based_on_parameter_mode(inputs, int(opcode_raw_values[2]), opcode_instructions[2])

			if first_value != 0:
				instruction_pointer_index = second_value
			else:
				instruction_pointer_index += 3

		elif opcode_instruction == 6:
			first_value = Intcode_computer.get_value_based_on_parameter_mode(inputs, int(opcode_raw_values[1]), opcode_instructions[1])
			second_value = Intcode_computer.get_value_based_on_parameter_mode(inputs, int(opcode_raw_values[2]), opcode_instructions[2])

			if first_value == 0:
				instruction_pointer_index = second_value
			else:
				instruction_pointer_index += 3

		elif opcode_instruction == 7:
			first_value = Intcode_computer.get_value_based_on_parameter_mode(inputs, int(opcode_raw_values[1]), opcode_instructions[1])
			second_value = Intcode_computer.get_value_based_on_parameter_mode(inputs, int(opcode_raw_values[2]), opcode_instructions[2])

			value_to_store = 0
			if first_value < second_value:
				value_to_store = 1

			inputs[int(opcode_raw_values[3])] = str(value_to_store)
			instruction_pointer_index += 4

		elif opcode_instruction == 8:
			first_value = Intcode_computer.get_value_based_on_parameter_mode(inputs, int(opcode_raw_values[1]), opcode_instructions[1])
			second_value = Intcode_computer.get_value_based_on_parameter_mode(inputs, int(opcode_raw_values[2]), opcode_instructions[2])

			value_to_store = 0
			if first_value == second_value:
				value_to_store = 1

			inputs[int(opcode_raw_values[3])] = str(value_to_store)
			instruction_pointer_index += 4

		return Intcode_computer.alter_input_by_opcode(inputs, instruction_pointer_index)