# https://adventofcode.com/2019/day/8

def get_inputs():
	with open('inputs.txt') as file:
		return file.read()

def get_image(pixels_wide, pixels_tall):
	inputs = get_inputs()

	pixels_per_layer = pixels_wide * pixels_tall
	layer_count = int(len(inputs) / pixels_per_layer)

	layers = []

	for i in range(0, layer_count):
		layers.append(inputs[pixels_per_layer * i:pixels_per_layer * (i + 1)])

	rows_count = int(len(layers[0]) / pixels_wide)
	columns_count = int(len(layers[0]) / pixels_tall)
	for i in range(0, len(layers)):
		rows = []
		for j in range(0, rows_count):
			rows.append(layers[i][columns_count * j:columns_count * (j + 1)])

		layers[i] = rows

	return layers

def assignment_a():
	image = get_image(25, 6)

	fewest_zero_index = 0
	fewest_zero_count = float("inf")

	for layer in image:
		zero_count = 0

		for row in layer:
			for pixel in row:
				if pixel == '0':
					zero_count += 1

		if zero_count < fewest_zero_count:
			fewest_zero_count = zero_count
			fewest_zero_index = image.index(layer)

	one_count = 0
	two_count = 0
	for row in image[fewest_zero_index]:
		for pixel in row:
			if pixel == '1':
				one_count += 1
				continue
			if pixel == '2':
				two_count += 1
				continue

	print(one_count * two_count)

def assignment_b():
	image = get_image(25, 6)

	total_layer = image[len(image) - 1]
	for layer in reversed(image):
		for i in range(0, len(layer)):
			row = layer[i]
			for j in range(0, len(row)):
				pixel = row[j]
				
				if pixel == '0' or pixel == '1':
					line = list(total_layer[i])
					line[j] = pixel
					total_layer[i] = "".join(line)


	print(*total_layer, sep = '\n')

assignment_a()
assignment_b()