# https://adventofcode.com/2019/day/7

from intcode_computer import Intcode_computer

import itertools

def get_inputs():
	inputs = []
	with open('inputs.txt') as file:
		for line in file:
			current_line = line.split(',')
			for value in current_line:
				inputs.append(value)
	return inputs

def get_amplifier_output(phase_settings):
	output = 0
	for phase_setting in phase_settings:
		computer = Intcode_computer()
		output = computer.run_program(get_inputs(), phase_setting, output)

	return output

def assignment_a():
	phase_setting_inputs = (0, 1, 2, 3, 4)

	possible_phase_settings = []
	for x in itertools.permutations(phase_setting_inputs, len(phase_setting_inputs)):
			possible_phase_settings.append(x)

	highest_output = 0
	for phase_setting in possible_phase_settings:
		output = get_amplifier_output(phase_setting)
		if output > highest_output:
			highest_output = output

	print(highest_output)

# def assignment_b():
# 	Intcode_computer.alter_input_by_opcode(get_inputs(), 0)

assignment_a()
# assignment_b()