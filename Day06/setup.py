from node import Node
import re

def get_inputs():
	inputs = []
	with open('Inputs.txt') as file:
		for line in file:
			identifiers = line.split(')')
			inputs.append((identifiers[0], identifiers[1].replace('\n', '')))
	return inputs

def get_node(identifier, nodes):
	for node in nodes:
		if node.identifier == identifier:
			return node

	return None

def get_or_create_node(identifier, nodes):
	node = get_node(identifier, nodes)
	if node != None:
		return node

	node = Node(identifier)
	nodes.append(node)
	return node

def get_nodes():
	inputs = get_inputs()
	nodes = []

	for input in inputs:
		node_parent = get_or_create_node(input[0], nodes)
		node_child = get_or_create_node(input[1], nodes)

		node_child.set_parent(node_parent)

	return nodes

def assignment_a():
	nodes = get_nodes()

	total_distance = 0
	for node in nodes:
		total_distance += node.get_distance()
	return total_distance

def get_node_parents(node):
	current_node = node
	parents = []

	while True:
		parent_node = current_node.parent
		if parent_node is None:
			break

		parents.append(parent_node)
		current_node = parent_node

	return parents

def assignment_b():
	nodes = get_nodes()

	current_parent = get_node('YOU', nodes).parent
	current_parents = get_node_parents(current_parent)

	target_parent = get_node('SAN', nodes).parent
	target_parents = get_node_parents(target_parent)

	for x in current_parents:
		for y in target_parents:
			if x == y:
				return current_parents.index(x) + target_parents.index(y) + 2

print(assignment_a())
print(assignment_b())