class Node:

	parent = None
	identifier = ''

	def __init__(self, identifier):
		self.identifier = identifier

	def set_parent(self, parent):
		self.parent = parent

	def get_distance(self):
		if self.parent is None:
			return 0

		return self.parent.get_distance() + 1