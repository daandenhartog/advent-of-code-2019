def assignment_a():
	value = min_range + 1
	valid_passwords = 0

	while value < max_range:
		list_of_values = list(str(value))
		if has_no_decreasing_value(list_of_values) == True and has_subsequent_value(list_of_values) == True:
			valid_passwords += 1

		value += 1

	return valid_passwords

def has_no_decreasing_value(list_of_values):
	previous_value = int(list_of_values[0])
	for i in range(1, len(list_of_values)):
		current_value = int(list_of_values[i])
		if current_value < previous_value:
			return False
		
		previous_value = current_value

	return True

def has_subsequent_value(list_of_values):
	for i in range(1, len(list_of_values)):
		if list_of_values[i - 1] == list_of_values[i]:
			return True

	return False

def assignment_b():
	value = min_range + 1
	valid_passwords = 0

	while value < max_range:
		list_of_values = list(str(value))
		if has_no_decreasing_value(list_of_values) == True and has_no_bigger_than_duo_value(list_of_values) == True:
			valid_passwords += 1

		value += 1

	return valid_passwords

def has_no_bigger_than_duo_value(list_of_values):
	subsequent_values = []
	subsequent_values.append(1)	

	for i in range(1, len(list_of_values)):
		previous_value = list_of_values[i - 1]
		current_value = list_of_values[i]

		if current_value == previous_value:
			subsequent_values[len(subsequent_values) - 1] += 1
		else:
			subsequent_values.append(1)

	return 2 in subsequent_values

min_range = 372037
max_range = 905157

print(assignment_a())
print(assignment_b())